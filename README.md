# Report UNIX

Compte-rendu de TP d'Unix - Repain Paul - IATIC3 - 2019

GitLab repository :


gitlab.com/Poulposaure/unix


## TP1

### TD4

#### 3 - finger

Find a username for a login given.
Steps:
- put the /etc/passwd in the STDIN
- take out the line with the login
- parse the line to get the username (can be an empty string)

```bash
cat /etc/passwd | grep "$1" | sed "s/$1:..*:..*:..*:\(.*\):..*:..*/\1/g"
```

#### 4 - who

Show the users currently connected, alphabetically sorted.

```bash
who | sed "s/^\([][a-zA-Z0-9][a-zA-Z0-9]*\)..*/\1/" | uniq | sort
```

Display the number of users currently connected, alphabetically sorted.

```bash
who | sed "s/^\([][a-zA-Z0-9][a-zA-Z0-9]*\)..*/\1/" | uniq | wc -l
```

#### 5 - pretree

Show the file structure. Only the directories are shown.
Steps :
- print all sub-directories with `find` and the option `-type d`
- parse the stream, replace each directory, between the directory passed in argument and the last, by a space (for indentation)

```bash
find $1 -type d | sed -e "s/\.//g; s/[a-zA-Z0-9]*\// /g"
```


Test :

```bash
$ mkdir -p a/d/f a/e b/g c
$ ./pretree .

 a
  d
   f
  e
 b
  g
 c
```

### TD5

#### 1 - compile

Checking the parameters (compilation of source file).

```bash
if [ $# -eq 2 ]
then
    if ! [ -e $2 ]
    then
        echo "File doesn't exist $2."
        exit 0
    fi

    $1 $2
elif [ $# -eq 1 ]
then
    if ! [ -e $1 ]
    then
        echo "File doesn't exist $1."
        exit 0
    fi
    gcc $1
else
    echo 'Wrong number of arguments.'
fi
```

#### 2 - del

```bash
echo -n "Confirm removal of file $1 ? (OoYy/Nn) "
read confirmation
case $confirmation in
    [OoYy]*) rm $1;;
esac
```

### 3 - filetype

Shows type of file : ordinary, directory, symbolic link, pipe or socket.

```bash
for file in $@
do
    echo -en "$file\t\t"
    if [ -f $file ]
    then
        echo File
    elif [ -d $file ]
    then
        echo Directory
    elif [ -L $file ]
    then
        echo 'Symbolic link'
    elif [ -p $file ]
    then
        echo Pipe
    elif [ -S $file ]
    then
        echo Socket
    fi
done
```


#### 4 - pman

`man`-like command using `less`.


```bash
location="/usr/share/man/fr/"

if [ $# -eq 2 ]
then
    location="$location/man$2/"
fi

man_page=$(find $location -name "$1*")

if [ "$man_page" = '' ]
then
    echo 'File not found.'
    exit 0
fi

zcat "$man_page" | more
```




Test :

```
$ ./pman zic 8
.\" %%%LICENSE_START(PUBLIC_DOMAIN)
.\" This page is in the public domain
.\" %%%LICENSE_END
.\"
.\"*******************************************************************
.\"
.\" This file was generated with po4a. Translate the source file.
.\"
.\"*******************************************************************
.TH ZIC 8 "25 février 2010" "" "Manuel de l'administrateur Linux"
.SH NOM
```
...

#### 5 - casio

A calculating machine. The previous result can be summoned by typing 'L'.

```bash
res=0
while [ 0 ]
do
    echo -n 'Operator : '
    read op
    echo -n 'First operand : '
    read l
    echo -n 'Second operand : '
    read r

    if [ "$l" = 'L' ]
    then
        l=$res
    fi
    if [ "$r" = 'L' ]
    then
        r=$res
    fi

    res=$(expr $l "$op" $r)
    echo "=$res"
done
```

Test :

```
$./casio
Operator :
First operand : 1
Second operand : 2
=3
Operator : *
First operand : 2
Second operand : 2
=4
```




### TD6

#### 3 - subdirs

Lists all sub-directories.

```bash
find "$1" -mindepth 1 -type d | sed "s/^..*\/\(..*\)$/\1/g"
```


#### 4 - display

`cat`-like command. To show spaces and tabs at beginning of line, the IFS is set to an empty string.

```bash
IFS=''
while read line
do
    echo $line
done < $1
```

#### 5 - rename

Rename all files passed in the arguments, with a prefix and an index.
Steps :
- if there is no file given, we take the whole directory
- we loop through the files and check if there are ordinary
- then, we change the name of the file with `mv` with the prefix and the index

```bash
i=1
prefix=$1

if [ $# -eq 1 ]
then
    files=*
else
    shift
    files=$@
fi

for file in $files
do
    if [ -f $file ]
    then
        new_name="$prefix$i"
        mv $file $new_name
        i=$(expr $i + 1)
    fi
done
```


Test :

```
$ touch a b c d
$ ./rename poulet a b c d
$ ls poulet*
poulet1  poulet2  poulet3  poulet4
```

## TP2

### TD7

#### 1 - slay

Kill a process by his command name.
Steps :
- lists all processes with `ps`
- get the info/line of the process with `grep`
- remove a space before the PID with `sed`
- get the PID with `cut`
- `kill` the process with the PID

```bash
process=$(ps | grep $1 | sed "s/^ \(.*\)$/\1/g" | cut -d ' ' -f 1)


if [ ${#process} -eq 0 ]
then
    echo 'No process found.'
    exit 0
fi

kill -9 $process
```

### 2 - pattern
Find a pattern in files under a directory.

Steps :

- find the (ordinary) files with the `find` command
- parse each file with the pattern with `xargs` and `grep`

```bash
find $1 -type f | xargs grep -o $2
```


#### 3 - intercc

Interactive C compilation.
Steps:
- compile the C file with `cc`, alias of `gcc`
- store the first error string with `head` and the option `-n1`. It is an error that is returned by cc, thus it is from the standard error that we must take out the string, by doing : `2>&1`
- from string, take out the line and column number where the error is located
- open the file with `vi` and execute a command with the option `-c` : move the cursor to the right place and go in insert mode

```bash
error=$(cc $1 2>&1 | grep error | head -n1 )
nums=$(echo $error | sed "s/^$1:\([0-9]*:[0-9]*\)..*$/\1/g")
col=$(echo "$nums" | cut -d ':' -f 1)
line=$(echo "$nums" | cut -d ':' -f 2)

echo $error
echo 'Type any key to continue.'
read any_key

vi -c ":call cursor($line,$col)|startinsert" $1
```

Test :

```
$ ./intercc test.c
test.c:2:2: error: invalid preprocessing directive #DEFINE
Type any key to continue.
```

#### 4 - editv

Versionning : editing a file will create a new with an incremented version.

Steps :
- take out the file version with `cut`
- create the new file name with the incremented file version
- copy the content of the older file into the newer file with `cp`
- open the new file with `vi`


```bash
version=$(echo $1 | cut -d '.' -f 2)
next_version=$(expr 1 + $version)
new_filename="$(echo $1 | cut -d '.' -f 1).$next_version"
cp $1 $new_filename
vi $new_filename
```

The following script is for removing all version of a file except the newest/latest.
Steps :
- count the number of files versionned with `wc`
- loop through the files, sorted by their version with the option `--sort=v`. Each file is removed except the last one

```bash
i=1
files_count=$(ls $1* | wc -w)
for file in $(ls $1* --sort=v)
do
    if [ $i -eq $files_count ]
    then
        exit 0
    fi
    rm $file
    i=$(expr $i + 1)
done
```

#### 7

```
$ touch a.x b.x c.x b.y d.y
$ mv *.x *.y
mv: la cible 'd.y' n'est pas un répertoire
```


### TD8

#### 1 - ftp

```
$ ftp ftp.uvsq.fr
Connected to nova.uvsq.fr.
220-
220-             -- Bienvenue sur le serveur ftp de l'UVSQ ---
220-Utilisez le compte `anonymous' avec votre adresse e-mail comme mot de passe
220-    Merci de signaler les problemes eventuels a ftpmaint@uvsq.fr.
220-
220-               -- Welcome on the UVSQ ftp site --
220-    Please login as `anonymous' with your e-mail address as password
220-            Please report problems to ftpmaint@uvsq.fr.
220-
220-
220 ftp.uvsq.fr FTP server ready.
530 Please login with USER and PASS.
530 Please login with USER and PASS.
KERBEROS_V4 rejected as an authentication type
Name (ftp.uvsq.fr:21700215): anonymous
331 Guest login ok, send your complete e-mail address as password.
Password:
230 Guest login ok, access restrictions apply.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> cd pub/gcc
250-Please read the file README
250-  it was last modified on Fri Apr 25 08:09:05 2003 - 5984 days ago
250 CWD command successful.
ftp> get README
local: README remote: README
227 Entering Passive Mode (193,51,24,2,163,46)
150 Opening BINARY mode data connection for README (765 bytes).
226 Transfer complete.
765 bytes received in 0.008 seconds (93 Kbytes/s)
ftp> 221-You have transferred 765 bytes in 1 files.
221-Total traffic for this session was 1848 bytes in 1 transfers.
221-Thank you for using the FTP service on ftp.uvsq.fr.
221 Goodbye.
```

#### 2 - ssh

xclock works with the option `-X`, which activates the X11 forwarding :

```bash
ssh -X 21802524@einstein.isty.uvsq.fr
```


#### 3 - scp

With `scp` :
```
$ mkdir 21700215; touch 21700215/A.txt
$ scp A.txt 21700215@darwin.isty.uvsq.fr:~
21700215@darwin.isty.uvsq.fr's password:
A.txt                                                  100%   18     0.0KB/s   00:00
```

With `rsync` :
```
$ rsync 21700215/A.txt 21700215@darwin.isty.uvsq.fr:~/
```

#### 4 - rsync

The `rsync` command both ways with the option `-u` and synchronize directories with `-r`.


### 2 - XWindow

```
$ echo $DISPLAY
athenes.isty.uvsq.fr:1.0
$ export DISPLAY=athenes.isty.uvsq.fr
$ xclock
Error: Can't open display: athenes.isty.uvsq.fr
$ export DISPLAY=berlin.isty.uvsq.fr:1.0
[21700215@darwin ~]$ xclock
Xlib: connection to "berlin.isty.uvsq.fr:1.0" refused by server
Xlib: No protocol specified

Error: Can't open display: berlin.isty.uvsq.fr:1.0
```

After a scp for getting ~/.Xauthority, the other machine displays xclock. The copied file has the same permissions.

### 4

Returns the links between processes :

```bash
ps uf | tail -n +2 | sed "s/^..* [0-9]:[0-9][0-9] \(..*\)$/\1/g"
```

Output :
```
bash
 \_ ps uf
 \_ tail -n +2
 \_ sed s/^..* [0-9]:[0-9][0-9] \(..*\)$/\1/g
/usr/lib/gdm3/gdm-x-session --run-script default
 \_ /usr/lib/xorg/Xorg vt2 -displayfd 3 -auth /run/user/1000/gdm/Xauthority -background none -noreset -keeptty -verbose 3
 \_ /usr/lib/gnome-session/gnome-session-binary
     \_ /usr/bin/gnome-shell
     \_ /usr/lib/gnome-settings-daemon/gnome-settings-daemon
     \_ /usr/lib/tracker/tracker-extract
     \_ /usr/lib/tracker/tracker-miner-apps
     \_ /usr/lib/tracker/tracker-miner-fs
     \_ /usr/lib/tracker/tracker-miner-user-guides
     \_ /usr/lib/evolution/evolution-alarm-notify
     \_ /usr/bin/gnome-software --gapplication-service
/usr/lib/gnome-settings-daemon/gsd-printer
```

## Micro-projects

### 1 - mirror

Directories synchronization.
Steps :

- first, loop through all erased files. Erased files matches the pattern *~
- if the other directory has the same file but not erased, we add a ~ to the filename to mark it as erased
- second, loop through all files
- check the file doesn't exist in the other directory
- if so, create the directories (if they were non-existent) and copy the file. The date is kept from the source file with the option `--preserve`
- if the file exists, we copy the file with `cp` and the `-u` option that updates the existent file if it is older than the source
- repeat the same process with the other repository

```bash
for file in $(find $1 -name "*~")
do
    filename=$(basename $file)
    filename=${filename::-1}
    f2=$(find $2 -name "$filename")
    if [ "$f2" != '' ]
    then
        mv $f2 "$f2~"
    fi
done

for file in $(find $1 -type f)
do
    f2=$(find $2 -name $(basename $file))
    if [ "$f2" = '' ]
    then
        p="$2/$(path $1 $file)"
        mkdir -p $p && cp --preserve $file $p
    else
        cp -u --preserve $file $f2
    fi
done
```

Function returning the path between a directory and a file
```bash
path() {
    filename=$(basename $2)
    echo $(echo $2 | sed "s/$1\/\(.*\)$filename/\1/g")
}
```

### 2 - consult

Phonebook. The path to the executable for synchronization must be provided in the file. The executable must be run into the phonebook directory.
Option :
-s=[PATH]
Will synchronize the phonebook (before quitting) with another phonebook with a path provided.

```bash
sync_path="../mirror"

main() {
    while [ "$choice" != 'q' ]
    do
        echo 'Phone book'
        echo '* * * * * * * * * * * * *'
        echo '1 - Add a form'
        echo '2 - List all saved names'
        echo '3 - Search a phone number'
        echo '4 - Change a phone number'
        echo '5 - Search a name'
        echo '6 - Synchronize phonebooks'
        echo 'q - Quit'
        echo '* * * * * * * * * * * * *'
        echo -n 'Your choice : '
        read choice
        case "$choice" in
            1) add_form;;
            2) name_list;;
            3) search_phone_number;;
            4) change_phone_number;;
            5) search_form;;
            6) sync_phonebook;;
            q)
                if [ $# -eq 1 -a "${1:1:1}" = 's' ]
                then
                    d="${1#-s=}"
                    $("$sync_path" . "$d") && echo "Synchronization successful with $d"
                fi
                exit 0;;
        esac
    done

}

sync_phonebook() {
    echo -n 'Location of directory to synchronize : '
    read dir
    $("$sync_path" . "$dir") && echo "Synchronization successful with $dir"
}

add_form() {
    echo -n 'Name : '
    read name
    echo -n 'Telephone number : '
    read number

    echo "$number" > "$name"
}

name_list() {
    ls *[^~]
}

search_phone_number() {
    echo -n 'Name : '
    read name

    if [ -e "$name" ]
    then
        cat "$name"
    else
        echo "The form doesn't exist for $name."
    fi
}

change_phone_number() {
    echo -n 'Name : '
    read name

    echo -n 'New phone number : '
    read number

    if [ -e "$name" ]
    then
        echo "$number" > "$name"
    else
        echo "The form doesn't exist for $name."
    fi
}

search_form() {
    echo -n 'Phone number : '
    read number

    for form in $(ls *[^~])
    do
        if [ $number = $(cat $form) ]
        then
            echo "$form"
            return
        fi
    done

    echo "No name found for the number $number."
}

main $1
```


### 3 - rpn

A calculating machine supporting the Reverse Polish Notation (RPN).

The input is read either from a file either from the `STDIN` with the command : `${1:-/dev/stdin}` (if $1 is null, $1 becomes the `STDIN`). You can execute the file and give the input to the interpreter, or call the interpreter in a bash script and write the input.
Steps :
- `read` a line till the user enters 'q'. The operators +, -, /, * and \\* are supported
- we read the character. If a token is a number, it is stored; if it is an operator, `expr` is called with the last 2 operands stored and the operator


```bash
while read operation
do
    if [ "$operation" = 'q' ]
    then
        exit 0
    fi

    for e in "$operation"
    do
        if [[ "$e" == [\+\-\*/] || "$e" == '\*' ]]
        then
            echo "$x $e $y"
            x=$(expr $x "$e" $y)
            echo "= $x"
            y=$z
            z=$t
            t=0
        else
            t=$z
            z=$y
            y=$x
            x=$e
        fi
    done

done < "${1:-/dev/stdin}"
```

Test with the magic line :
```
#!./rpn
1
2
+
4
4
-
56
+
1
+
```

Output :
```
2 + 1
= 3
4 - 4
= 0
56 + 0
= 56
1 + 56
= 57
```
